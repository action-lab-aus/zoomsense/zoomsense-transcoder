const path = require("path");
const fs = require("fs");
const { bucket, recordingsRef, activeSpeakerRef } = require("./db/admin");
const { runTranscode } = require("./transcode");

const ip = "35_201_13_190";
let isProcessingVideo = false;
let rawVideoPath;

(async () => {
  try {
    fs.mkdirSync(path.join(".", "tmp"), true);
  } catch (err) {
    // Already exists
  }

  // Check for more queue every 30 seconds
  await checkRecordings();
  setInterval(async function() {
    await checkRecordings();
  }, 30 * 1000);
})();

async function checkRecordings() {
  if (!isProcessingVideo) {
    isProcessingVideo = true;

    let latest = await recordingsRef
      .child(ip)
      .orderByKey()
      .limitToFirst(1)
      .once("value");

    console.log("Number of Children: ", latest.numChildren);

    if (latest.numChildren() === 1) {
      const key = Object.keys(latest.val())[0];
      const file = latest.child(key).val();

      // Remove the last bit of the file path and get all files from bucket
      let dir = file.filePath.split("/");
      dir.pop();
      rawVideoPath = dir.join("/");
      const [files] = await bucket.getFiles({
        prefix: rawVideoPath,
      });

      console.log(`Start downloading raw recording files for ${rawVideoPath}`);

      // Download all files in the following directory
      for (let file of files)
        await file.download({
          destination: path.join(".", "tmp", file.name.split("/").pop()),
        });

      console.log(
        `Raw recording files downloaded, start transcoding for ${rawVideoPath}`
      );

      // Run video transcoder container for the .meetingrec files
      const tmpPath = path.join(__dirname, "tmp");
      const transcoderPath = path.join(__dirname, "bin/zTscoder.exe");
      runTranscode(transcoderPath, tmpPath);

      // Wait until the .mp4 file exists or the container exits
      const finalVideoPath = path.join(__dirname, "tmp/meetingrec_0.mp4");
      const timeout = setInterval(async function() {
        const fileExists = fs.existsSync(finalVideoPath);
        if (fileExists) {
          console.log(
            `Transcoding finished, start uploading .mp4 file for ${rawVideoPath}`
          );
          clearInterval(timeout);

          // Upload to Firebase Bucket
          await bucket.upload(finalVideoPath, {
            destination: rawVideoPath + "/meetingrec_0.mp4",
          });

          // Remove all local files
          let retryCount = 1;
          while (retryCount <= 10) {
            const success = removeTmpFiles(tmpPath);
            if (success) break;
            else {
              await sleep(5000);
              retryCount++;
            }
          }

          await updateTranscodingQueue(ip, key);
          isProcessingVideo = false;
        }
      }, 10 * 1000);
    } else isProcessingVideo = false;
  }
}

function removeTmpFiles(tmpPath) {
  console.log(
    `Uploading finished, start cleaning temp files for ${rawVideoPath}`
  );

  try {
    const files = fs.readdirSync(tmpPath);
    files.forEach((file) => {
      fs.unlinkSync(path.join(tmpPath, file));
    });
    return true;
  } catch (error) {
    return false;
  }
}

async function updateTranscodingQueue(ip, key) {
  console.log(
    `Cleaning temp files finished, start updating Firebase data for ${rawVideoPath}`
  );

  try {
    // Update transcoding information to Firebase
    const filePathArr = rawVideoPath.split("/");
    const meetingId = filePathArr[1];
    const sensorId = filePathArr[2].split("-")[0];
    const timestamp = filePathArr[2].split("-")[1];
    await activeSpeakerRef
      .child(`${meetingId}/${sensorId}/history/${timestamp}/finishTranscode`)
      .set(true);

    // Remove from processing queue
    await recordingsRef
      .child(ip)
      .child(key)
      .remove();
  } catch (error) {
    console.error(error);
  }
}
