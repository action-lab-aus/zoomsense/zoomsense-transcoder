const admin = require("firebase-admin");
const serviceAccount = require("../../zoombot-firebase-adminsdk.json");

// Initialize Firebase application
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://zoombot.firebaseio.com",
  storageBucket: "zoombot.appspot.com",
});

const db = admin.database();
const bucket = admin.storage().bucket();
const recordingsRef = db.ref("processing/recordings");
const activeSpeakerRef = db.ref("data/activeSpeakers");

exports.bucket = bucket;
exports.recordingsRef = recordingsRef;
exports.activeSpeakerRef = activeSpeakerRef;
