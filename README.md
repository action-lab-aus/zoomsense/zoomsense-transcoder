# ZoomSense Transcoder

This repository contains a local script for running transcoding functions for ZoomSense recordings. To minimize the server workload, all the Zoom meeting recordings will be pushed to Firebase Storage in their raw formats (`.meetingrec`). The transcoder Docker container will fetch the jobs from the queue and transcode the recordings into `.mp4` format for future analysis and access.

Since the transcoding job is resource-intensive, we are currently running transcoding jobs using this local script. However, the Docker transcoder container can also be integrated with a high-spec cloud VM to start transcoding jobs automatically.

# Getting Started

## 1. Download the Zoom SDK

Log in to the [Zoom App Marketplace](https://marketplace.zoom.us/) using your Zoom account, click the **Develop** option in the dropdown on the top-right corner and select **Build App**. Next, click the **Create** button and provide the required details if you haven’t already created an SDK app. If you previously created an SDK app on the Marketplace, click the **View here** option and navigate to the **Download** page. Click **Windows(C#)** to download the Windows(C#) SDK.

## 2. Extract the `bin` Directory with `zTsCoder.exe`

The transcoding script uses `zTsCoder` from Zoom to transcode recordings. Extract the C# SDK and put the `bin` directory under this project root.

## 3. Add Firebase Credentials

Under `db/admin.js`, update your path to the Firebase Service Account for ZoomSense to get access to the transcoding job queue and start the transcoding tasks.

## 4. Start Transcoding

Install dependencies:

```
npm install
```

Start transcoding jobs:

```
node index.js
```

The script will check whether there are any transcoding jobs remaining in the queue. If yes, it will start transcoding for raw recordings and push the `.mp4` file back to Firebase Storage upon completion.
