/**
 * Create and execute transocder process
 * @param {*} transcoderPath Path of the zTscoder.exe file
 * @param {*} path Path of the .meetingrec files to be transcoded
 */
async function runTranscode(transcoderPath, path) {
  var spawn = require("child_process").spawn,
    child;

  child = spawn("powershell.exe", [`${transcoderPath} ${path}`]);
  child.stdin.end();
}

exports.runTranscode = runTranscode;
